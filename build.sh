#!/bin/bash -ex

# CONFIG
prefix="Cyberduck"
suffix=""
munki_package_name="Cyberduck"
display_name="Cyberduck"
icon_name=""
category="Utilities"
description="This update contains stability and security fixes for Cyberduck."
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.zip -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# unzip
unzip app.zip

#Obtain variables from Application
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" *.app/Contents/Info.plist`
minOS=`/usr/libexec/PlistBuddy -c "Print :LSMinimumSystemVersion" *.app/Contents/Info.plist`

#Copy app to build-root
mkdir -p build-root/Applications
mv Cyberduck.app build-root/Applications
#hdiutil create -srcfolder app_tmp -size 1GB -format UDZO -o app.dmg

#build PKG
#/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --scripts scripts --version "$version" app.pkg

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

#clean up the component file
rm -rf Component-${munki_package_name}.plist


#parse key files for use in makepkginfo
key_files=`find build-root -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

# Build pkginfo
/usr/local/munki/makepkginfo app.pkg ${key_files} > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Obtain version info for plist
#version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "${minOS}"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Add requires array
#Requires="JavaForMacOSX"
#defaults write "${plist}" requires -array "${Requires}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
