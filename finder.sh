#!/bin/bash


NEWLOC=`curl -L "https://cyberduck.io/download/" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .zip | tail -1 `

if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi
